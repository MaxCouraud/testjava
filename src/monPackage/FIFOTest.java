package monPackage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class FIFOTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }


    @Test
    public void add() {
        FIFO instFIFO = new FIFO();
        int lenghtQueue = instFIFO.size();
        instFIFO.add(1);
        //valeur attendu lenghtQueue
        assertEquals(lenghtQueue,lenghtQueue++);

        FIFO instTwoFIFO = new FIFO();
        instTwoFIFO.add(12);
        assertEquals(instTwoFIFO.first(),Integer.valueOf(12));
    }

    @Test
    public void first() {
    }

    @Test
    public void isEmpty() {
        FIFO instFIFO = new FIFO();
        assertEquals(instFIFO.isEmpty(), true);
    }

    @Test
    public void removeFirst() {
    }

    @Test
    public void size() {
        FIFO instFIFO = new FIFO();
        int taille = 1;
        instFIFO.add(55);
        assertEquals(instFIFO.size(), taille);

        FIFO instTwoFIFO = new FIFO();
        int tailleBis = 0;
        assertEquals(instTwoFIFO.size(), tailleBis);
    }

}
