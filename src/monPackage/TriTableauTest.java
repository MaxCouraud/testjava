package monPackage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class TriTableauTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void trier() {
    }

    @Test
    public void triCroissant() throws Exception {
        int tab[] = {2,4,5,6,8,1};
        int valid[] = {1,2,4,5,6,8};
        TriTableau.triCroissant(tab);
        assertEquals(valid[0],tab[0]);
    }

    @Test
    public void triDecroissant() throws Exception {
        int tab[] = {2,4,5,6,8,1};
        int valid[] = {8,6,5,4,2,1};
        TriTableau.triDecroissant(tab);
        assertEquals(valid[0],tab[0]);
    }

}
